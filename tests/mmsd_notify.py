#!/usr/bin/python3
import dbus

bus = dbus.SystemBus()

modem = bus.get_object('org.freedesktop.ModemManager1', '/org/freedesktop/ModemManager1/SMS/40')
bus_iface = dbus.Interface(modem, dbus_interface='org.freedesktop.DBus.Properties')

props = bus_iface.GetAll("org.freedesktop.ModemManager1.Sms")



array_de_bytes = dbus.Array([])
for byte in props['Data']:
  print("byte " +chr(byte))
  array_de_bytes.append(dbus.Byte(byte))

session_bus = dbus.SessionBus()
dbus_mmmms_object = session_bus.get_object('org.ofono.mms.ModemManager', '/org/ofono/mms')
dbus_mmmms_properties = dbus.Interface(dbus_mmmms_object, dbus_interface='org.ofono.mms.ModemManager')
sms_properties = dbus_mmmms_properties.PushNotify(array_de_bytes)
