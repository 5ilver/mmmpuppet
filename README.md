# MMMPuppet
Modem Manager Matrix Puppet. Bridge SMS/MMS messages between MM and
 Matrix.

## Goal
I'm just getting started with this. I'm testing on the pmOS PinePhone.

This will be a background service that runs on the PinePhone. It'll take
 SMS/MMS messages and bridge them to Matrix so you can text from any
 device with a matrix client.

It will interact with [Modem Manager](https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/)
 and [mmsd](https://gitlab.com/kop316/mmsd)
 directly to Send/Recieve SMS/MMS messages.

It will use [matrix-nio](https://matrix.org/sdks/#matrix-nio) python SDK
 to interact with Matrix.

I'll happily accept PRs for any reason. New features, bug fix, whatever.

## Status
**Cell -> Matrix**
* [X] SMS
* [X] MMS
* [X] Group Messages


**Matrix -> Cell**
* [X] m.text
* [X] m.image
* [X] m.XXXXX


**General**
* [ ] Read Contacts
* [ ] Ignore list
* [X] Initiate a new conversation in Matrix
* [ ] Retry failed messages to Matrix
* [ ] Retry failed messages to Cell Network
* [ ] Start on Boot
* [ ] Test with Deep Sleep

## TODO:

* Set Permissions to allow Name change (but not Topic)
* Clean up MMSD for sent mmses
* any todos in code
* listening on sms dbus interface needs to change if modem moves.
* change display name to match room name.
* wake modem???
* wait for all users to leave room before leaving.
* if new user spams sms messages several rooms will be created for them.

## Dependencies

* [mmsd](https://gitlab.com/kop316/mmsd)
* Python >= 3.7
* pip3 install --user matrix-nio phonenumbers vobject
* git clone https://gitlab.com/untidylamp/mmmpuppet.git


*NOTE* I'm pretty sure if chatty is running at the same time
 as this, it will case problems. We both try and clean up
 the messages on the modem.

## Setup

Make sure you have all the dependencies as listed above. Once you
 have this, copy the sample config file to $HOME/.config/mmm/conf.json
 and edit any of the variables to match the information for your setup

Once the config file is updated, Run the script:

    ./mmmpuppet.py

The first run will Login to the matrix server as the bot, update the
 config file to have the Token and Device ID, removing the password.
 It should print any errors to the screen, otherwise tail the logs
 for awhile and see if you have any problems.

    tail -f $HOME/.config/mmm/mmmpuppet.log

Run it a second time to start bridging.

If you use debug(10) level logging then usernames, passwords, phone
 numbers, and message content will be in the logs. To see what the app
 is doing without logging this stuff, Please use info(20). This is set
 in the script itself. Default is debug for now, I'll change it to 
 info later.



## Usage

To start a new chat, create a room in Matrix, Set the Name of the room
 to the name of the contact and the Topic to their phone number. If
 this is a group chat, Name the room and provide a CSV list of numbers
 in the topic. If the room creation was successful the bot will let you
 know. Messages you send here will be sent to that user via SMS/MMS.

Incomming messages will look for a room in the database. If it doesn't
 have one it will create a matrix room and invite the users. All new 
 messages for that room will be posted there. Any time you post in that
 room a message will be sent back to the user via SMS/MMS.

The "matrix_users_csv" in the config file is a csv list of people
 that you trust and want to be able to use this cellphone number.
 Only the users accounts listed here will be bridged to SMS/MMS
 and each of them will be invited to a new room the bot creates.
 This way PersonA and PersonB can share a cellphone number with their
 own matrix accounts. 

Read Receipts are moved after a message is sent to MM/MMSD for delivery.

When you change the name of the room, the bot will change their display
 name (TODO)

This script will clean up MM and MMSD messages as their bridged.


## Mobian from scratch.

Mobian seems to be the most popular OS right now so I'll go though the setup from scratch below.

**Desktop**

    wget https://images.mobian-project.org/pinephone/nightly/mobian-pinephone-phosh-20210207.img.gz

    gunzip mobian-pinephone-phosh-20210207.img.gz

    sudo dd if=./mobian-pinephone-phosh-20210207.img of=/dev/sdX status=progress

Take the SD Card out of your PC and put into your PinePhone.

Boot the phone, Login with 1234

go thought he setup menu. If you didn't setup WiFi, do so now. 
settings -> WiFi

Make sure mobile data is enabled and working.

Settings -> Mobile -> Enable Mobile Data -> Select / Add your AP.


Open King's Cross app. Get our IP, if you want to ssh in to the phone.

    ip a s wlan0

Install openssh-server so we can ssh into phone (optional)

    sudo su - 
    apt install openssh-server
    systemctl enable ssh
    systemctl start ssh

On Desktop ssh into phone or in King's Cross directly.

    sudo apt install git
    git clone https://gitlab.com/untidylamp/mmmpuppet.git
    git clone https://gitlab.com/kop316/mmsd.git

    sudo apt install make automake build-essential libtool libglib2.0-dev libdbus-glib-1-dev libmm-glib-dev python3-pip meson

    cd mmsd
    meson _build
    meson compile -C _build
    meson test -C _build
    sudo meson install -C _build

    /usr/local/bin/mmsd
    ps aux | grep mmsd #make sure its running
    killall mmsd
    vi ~/.mms/modemmanager/ModemManagerSettings

set it up for your network then restart mmsd.

    /usr/libexec/mmsd
    ps aux | grep mmsd

get rid of chatty.

    killall chatty
    ps aux | grep chatty

    pip3 install --user matrix-nio
    pip3 install --user phonenumbers

    cd ~/mmmpuppet/
    mkdir -p ~/.config/mmm/
    cp conf.json.sample ~/.config/mmm/conf.json
    vi ~/.config/mmm/conf.json

Add your bot account information. The password will be removed after the first run.

    cd ~/mmmpuppet/
    ./mmmpuppet.py
    tail -f ~/.config/mmm/mmmpuppet.log



